#pragma once
#include "MLClass.h"
#include "Frame.h"

class AgeClassifier : public MLClass
{
private:
	Frame frame;
	
public:
	AgeClassifier();
	int interpret_output(Mat& output);
	
	int classify_age(string path);
};
