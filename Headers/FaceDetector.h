#ifndef FACEDETECTOR_H
#define FACEDETECTOR_H

#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "Frame.h"

#include <vector>
#include <string>

using namespace cv;

class FaceDetector
{
private:
	CascadeClassifier face_cascade;
	CascadeClassifier eyes_cascade;
	
public:
	FaceDetector();
	
	void detect_faces(Frame frame, std::vector<Mat>& faces);
};

#endif
