#ifndef FACERECOGNITION_H
#define FACERECOGNITION_H

#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace cv::face;
using namespace std;

class FaceRecognition
{
private:
	Ptr<LBPHFaceRecognizer> model;
	
public:
	FaceRecognition();
	void train(vector<Mat> images, vector<int> labels);
	void save();
	void load();
	void update(vector<Mat> images, vector<int> labels);
	void predict(Mat sample);
	Mat process_img(InputArray src);
	
	int prediction;
	double confidence;
};

#endif
