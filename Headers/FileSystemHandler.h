//
// Created by vik on 20.09.2017.
//

#ifndef MLPROJECT_FILESYSTEMHANDLER_H
#define MLPROJECT_FILESYSTEMHANDLER_H

#include <iostream>
#include <experimental/filesystem>
#include <vector>
#include <string>

class FileSystemHandler
{
public:
    static std::vector<std::string> get_files_from_directory(std::string directory_path);
    static void delete_files_from_directory(std::string directory_path);
};

#endif //MLPROJECT_FILESYSTEMHANDLER_H
