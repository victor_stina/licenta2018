#ifndef FRAME_H
#define FRAME_H

#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"


#include <string>

class Frame
{
private:
	cv::Mat frame_mat;
	int height;
	int width;

public:
	Frame();
	Frame(cv::Mat frame_mat);
	Frame(const Frame& frame);

	void load_image(std::string path);
	void convert_to_rgb();
	void convert_to_gray();
	void resize(int new_width, int new_height);
	
	cv::Mat get_frame();
	void set_frame(cv::Mat frame);

	void operator=(const Frame& frm)
	{
		frame_mat = frm.frame_mat.clone();
		height = frm.height;
		width = frm.width;
	}
	
	void operator=(const cv::Mat& mat)
	{
		frame_mat = mat.clone();
		height = mat.rows;
		width = mat.cols;
	}		
};

#endif
