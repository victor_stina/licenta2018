#pragma once
#include "MLClass.h"
#include "Frame.h"

class GenderClassifier : public MLClass
{
private:
	Frame frame;
	
public:
	GenderClassifier();
	int interpret_output(Mat& output);
	
	int classify_gender(string path);
};
