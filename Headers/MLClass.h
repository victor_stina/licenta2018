#ifndef MLCLASS
#define MLCLASS

#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utils/trace.hpp>
#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/objdetect/objdetect.hpp"
using namespace cv;
using namespace cv::dnn;

#include <fstream>
#include <iostream>
#include <cstdlib>

using namespace std;

class MLClass
{
protected:
	Net net;
	String model_txt;
	String model_bin;
	
public:
	MLClass(String model_txt, String model_bin);
	
	Mat pass_blob_to_net(Mat& blob);
	Mat adjust_face(const Mat img, Rect rect);
	virtual int interpret_output(Mat& output) = 0;
};

#endif
