#pragma once
#ifdef _WIN32

#include <winsock2.h>
#include <Ws2tcpip.h>

#else

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

typedef int SOCKET;

#endif

#include <string>
#include "RecognitionHandler.h"
#include "GenderClassifier.h"
#include "AgeClassifier.h"

#define OK_STATUS 0
#define SERVER_INIT_SOCKET_ERROR -1
#define SERVER_CLOSE_SOCKET_ERROR -2
#define SERVER_SEND_DATA_ERROR -3
#define SERVER_RECEIVE_DATA_ERROR -4

enum
{
	SUCCESS = 0,
	LOGIN_AUTH_ERROR,
	LOGIN_PATH_ERROR,
	REGISTRATION_ERROR
};

using namespace std;

typedef int result;

enum
{
	LOGIN_REQUEST = 1,
	REGISTER_REQUEST,
	GENDER_REQUEST,
	AGE_REQUEST
};

struct data_packet
{
	string path;
	int action;
	int id;
};

class RServer
{
private:
	sockaddr_in server_address, client_address;
	SOCKET server_socket, client_socket;

public:
	RServer();
	RServer(int port_number);
	~RServer();

	result socket_init();	
	result socket_quit();
	result socket_close(SOCKET socket);

	void event_loop();
	result read_data(SOCKET socket, char* buffer, int count);
	result send_data(SOCKET socket, char* buffer, int count);
	data_packet parse_packet(char* input_string);
	std::string exec(const char* cmd);
};
