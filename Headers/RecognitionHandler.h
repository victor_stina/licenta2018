#pragma once

#include "Frame.h"
#include "FaceDetector.h"
#include "FaceRecognition.h"
#include "FileSystemHandler.h"

class RecognitionHandler
{
private:
	FaceDetector face_detector;
	FaceRecognition face_recognizer;
	
public:
	RecognitionHandler();
	
	int execute_recognition_sequence(std::string path, int label);
	int register_user(std::string path, int label);
};
