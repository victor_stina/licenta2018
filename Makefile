CC=g++
CFLAGS=-c -Wall -std=c++17 -g
LDFLAGS=`pkg-config --cflags --libs opencv` -pthread
INCLUDES=-IHeaders/
SOURCES=$(wildcard Sources/*.cpp)
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=bin/MLProject

all: $(SOURCES) $(EXECUTABLE)
	    
$(EXECUTABLE): $(OBJECTS) 
	    $(CC) $(INCLUDES) $(OBJECTS) -o $@ $(LDFLAGS) -lstdc++fs

.cpp.o:
	$(CC) $(INCLUDES) $(CFLAGS) $< -o $@ $(LDFLAGS) -lstdc++fs

clean:
	rm Sources/*.o $(EXECUTABLE)

