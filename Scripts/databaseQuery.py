#!/usr/bin/python
import MySQLdb
import sys

# Command: Age/Gender Value Id

db = MySQLdb.connect(host="localhost",  
                     user="Victor",        
                     passwd="Markof&cain1986", 
                     db="RecognitionDatabase",
                     unix_socket="/opt/lampp/var/mysql/mysql.sock")       


cur = db.cursor()
stmt = ""

if sys.argv[1] == "Gender":
    if sys.argv[2] == "1":
        stmt = "UPDATE utilizatori SET Gender='Female' WHERE id=" + sys.argv[3]
    else:
        stmt = "UPDATE utilizatori SET Gender='Male' WHERE id=" + sys.argv[3]

if sys.argv[1] == "Age":
    if sys.argv[2] == "1":
        stmt = "UPDATE utilizatori SET Age='0-15' WHERE id=" + sys.argv[3]
    if sys.argv[2] == "2":
        stmt = "UPDATE utilizatori SET Age='16-20' WHERE id=" + sys.argv[3]
    if sys.argv[2] == "3":
        stmt = "UPDATE utilizatori SET Age='21-26' WHERE id=" + sys.argv[3]
    if sys.argv[2] == "4":
        stmt = "UPDATE utilizatori SET Age='27-35' WHERE id=" + sys.argv[3]
    if sys.argv[2] == "5":
        stmt = "UPDATE utilizatori SET Age='36-45' WHERE id=" + sys.argv[3]
    if sys.argv[2] == "6":
        stmt = "UPDATE utilizatori SET Age='46-60' WHERE id=" + sys.argv[3]
    if sys.argv[2] == "7":
        stmt = "UPDATE utilizatori SET Age='61-70' WHERE id=" + sys.argv[3]
    if sys.argv[2] == "8":
        stmt = "UPDATE utilizatori SET Age='>70' WHERE id=" + sys.argv[3]

cur.execute(stmt)
db.commit()
db.close()