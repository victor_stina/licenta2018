#include "../Headers/AgeClassifier.h"
#include "../Headers/FileSystemHandler.h"
#include "../Headers/FaceDetector.h"

AgeClassifier::AgeClassifier() : MLClass("age_test.prototxt", "age.caffemodel")
{
	
}

int AgeClassifier::classify_age(string path)
{
	vector<string> subject_photos = FileSystemHandler::get_files_from_directory(path);
	frame.load_image(path + "/" + subject_photos[0]);
	
	CascadeClassifier face_cascade;
	face_cascade.load("/usr/local/share/OpenCV/lbpcascades/lbpcascade_frontalface_improved.xml");
	vector< Rect_<int> > faces_rect;
	face_cascade.detectMultiScale(frame.get_frame(), faces_rect);
	Mat face = MLClass::adjust_face(frame.get_frame(), faces_rect[0]);
	cv::resize(face, face, Size(224, 224), 1.0, 1.0, INTER_CUBIC);
	Mat inputBlob = blobFromImage(face, 1.0f, Size(224, 224),
										  Scalar(104, 117, 123), false);
	
	Mat prob = pass_blob_to_net(inputBlob);
	int ret = interpret_output(prob);
	return ret;
}

int AgeClassifier::interpret_output(Mat& output)
{
	double min, max;
	cv::Point min_loc, max_loc;
	cv::minMaxLoc(output, &min, &max, &min_loc, &max_loc);
	if (max_loc.x >= 0 && max_loc.x <= 15)
	{
		return 1;
	}
	else if (max_loc.x > 15 && max_loc.x <= 20)
	{
		return 2;
	}
	else if (max_loc.x > 20 && max_loc.x <= 26)
	{
		return 3;
	}
	else if (max_loc.x > 26 && max_loc.x <= 35)
	{
		return 4;
	}
	else if (max_loc.x > 35 && max_loc.x <= 45)
	{
		return 5;
	}
	else if (max_loc.x > 45 && max_loc.x <= 60)
	{
		return 6;
	} 
	else if (max_loc.x > 60 && max_loc.x <= 70)
	{
		return 7;
	}
	else
	{
		return 8;
	}
}
