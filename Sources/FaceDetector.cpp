#include "FaceDetector.h"

using namespace std;

FaceDetector::FaceDetector()
{
	face_cascade.load("/usr/local/share/OpenCV/lbpcascades/lbpcascade_frontalface_improved.xml");
	eyes_cascade.load("/usr/local/share/OpenCV/haarcascades/haarcascade_eye.xml");
}

void FaceDetector::detect_faces(Frame frame, vector<Mat>& faces)
{
	vector< Rect_<int> > faces_rect;
	face_cascade.detectMultiScale(frame.get_frame(), faces_rect);
	for (int i = 0; i < faces_rect.size(); i++)
	{
		Mat face = frame.get_frame()(faces_rect[i]);
		faces.push_back(face);
	}
}
