#include "FaceRecognition.h"

FaceRecognition::FaceRecognition()
{
	model = LBPHFaceRecognizer::create();
}
void FaceRecognition::train(vector<Mat> images, vector<int> labels)
{
	model->train(images, labels);
}

void FaceRecognition::save()
{
	model->write("../Model/FaceModel.yml");
}

void FaceRecognition::load()
{
	model->read("../Model/FaceModel.yml");
}

void FaceRecognition::update(vector<Mat> images, vector<int> labels)
{
	model->update(images, labels);
}

void FaceRecognition::predict(Mat sample)
{
	model->predict(sample, prediction, confidence);
}

Mat FaceRecognition::process_img(InputArray src) 
{
    Mat img = src.getMat();
    img.convertTo(img, CV_32FC1, (float)1.0/255);

    Mat processed_image;
    pow(img, 0.35, processed_image);

	Mat gaussian0, gaussian1;
	int kernel_sz0 = 3;
	int kernel_sz1 = 7;

	GaussianBlur(processed_image, gaussian0, Size(kernel_sz0,kernel_sz0), 1, 1, BORDER_REPLICATE);
	GaussianBlur(processed_image, gaussian1, Size(kernel_sz1,kernel_sz1), 2, 2, BORDER_REPLICATE);
	subtract(gaussian0, gaussian1, processed_image);

	double meanI = 0.0;
	Mat tmp;
	pow(abs(processed_image), 0.2, tmp);
	meanI = mean(tmp).val[0];
	processed_image = processed_image / pow(meanI, 1.0 / 0.2);

	meanI = 0.0;
	Mat tmp2;
	pow(min(abs(processed_image), 10.0), 0.2, tmp2);
	meanI = mean(tmp2).val[0];

	processed_image = processed_image / pow(meanI, 1.0 / 0.2);

	Mat exp_x, exp_negx;
	exp( processed_image / 10.0, exp_x );
	exp( -processed_image / 10.0, exp_negx );
	divide( exp_x - exp_negx, exp_x + exp_negx, processed_image );
	processed_image = 10.0 * processed_image;

    return processed_image;
}