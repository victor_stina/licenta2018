//
// Created by vik on 20.09.2017.
//

#include <vector>
#include "FileSystemHandler.h"

using namespace std;

vector<string> FileSystemHandler::get_files_from_directory(string directory_path)
{
	vector<string> paths;
	for (auto& p: std::experimental::filesystem::directory_iterator(directory_path.c_str()))
	{
		string file_path = p.path().filename().string();
		paths.push_back(file_path);
	}
	return paths;
}

void FileSystemHandler::delete_files_from_directory(string directory_path)
{

}

