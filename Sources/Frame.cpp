#include "../Headers/Frame.h"

using namespace std;
using namespace cv;

Frame::Frame()
{
	height = 0;
	width = 0;	
}

Frame::Frame(Mat frame_mat)
{
	this->frame_mat = frame_mat.clone();
	this->height = frame_mat.rows;
	this->width = frame_mat.cols;
}

Frame::Frame(const Frame& frame)
{
	this->frame_mat = frame.frame_mat.clone();
	this->height = frame.height;
	this->width = frame.width;
}

void Frame::load_image(std::string path)
{
	this->frame_mat = imread(path.c_str(), CV_LOAD_IMAGE_COLOR);
	this->height = this->frame_mat.rows;
	this->width = this->frame_mat.cols;
	convert_to_rgb();
}

void Frame::convert_to_rgb()
{
	cvtColor(frame_mat, frame_mat, CV_BGR2RGB);
}

void Frame::convert_to_gray()
{
	cvtColor(frame_mat, frame_mat, CV_RGB2GRAY);
}

void Frame::resize(int new_width, int new_height)
{
	cv::resize(frame_mat, frame_mat, Size(new_width, new_height), 1.0, 1.0, INTER_CUBIC);
}

cv::Mat Frame::get_frame()
{
	return frame_mat;
}

void Frame::set_frame(cv::Mat frame)
{
	frame_mat = frame.clone();
}
