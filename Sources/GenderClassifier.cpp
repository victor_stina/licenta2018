#include "../Headers/GenderClassifier.h"
#include "../Headers/FileSystemHandler.h"
#include "../Headers/FaceDetector.h"

GenderClassifier::GenderClassifier() : MLClass("gender_new.prototxt", "gender.caffemodel")
{
	
}

int GenderClassifier::classify_gender(string path)
{
	vector<string> subject_photos = FileSystemHandler::get_files_from_directory(path);
	cout << subject_photos[0] << endl;
	frame.load_image(path + "/" + subject_photos[0]);
	cout << "***********************************\n";
	cout << frame.get_frame().rows << " " << frame.get_frame().cols << endl;
	cout << "***********************************\n";
	CascadeClassifier face_cascade;
	face_cascade.load("/usr/local/share/OpenCV/lbpcascades/lbpcascade_frontalface_improved.xml");
	vector< Rect_<int> > faces_rect;
	face_cascade.detectMultiScale(frame.get_frame(), faces_rect);
	Mat face = MLClass::adjust_face(frame.get_frame(), faces_rect[0]);
	cv::resize(face, face, Size(224, 224), 1.0, 1.0, INTER_CUBIC);
	Mat inputBlob = blobFromImage(face, 1.0f, Size(224, 224),
										  Scalar(104, 117, 123), false);
	
	Mat prob = pass_blob_to_net(inputBlob);
	int ret = interpret_output(prob);
	return ret;
}

int GenderClassifier::interpret_output(Mat& output)
{
	float female_chance = output.at<float>(0,0);
	float male_chance = output.at<float>(0,1);
	
	if (female_chance > male_chance)
		return 1;
	else
		return 2;
}
