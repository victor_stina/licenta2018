#include "../Headers/MLClass.h"

MLClass::MLClass(String model_txt, String model_bin)
{
	this->model_txt = "../Model/" + model_txt;
	this->model_bin = "../Model/" + model_bin;
	
	net = dnn::readNetFromCaffe(this->model_txt, this->model_bin);
}

Mat MLClass::pass_blob_to_net(Mat& blob)
{
	Mat prob;
	for (int i = 0; i < 10; i++)
	{
		CV_TRACE_REGION("forward");
		net.setInput(blob, "data");
		prob = net.forward("prob");   
	}
	return prob;
}

Mat MLClass::adjust_face(const Mat img, Rect rect)
{
	int x, y, w, h;
	int x2 = -1, y2 = -1, w2 = img.cols + 1, h2 = img.rows + 1;
	x = rect.tl().x;
	y = rect.tl().y;
	w = rect.width;
	h = rect.height;
	float div = 1;
	while (x2 < 0)
	{
		x2 = x - w/div;
		cout << "Adjusting top left x coord: " << x << " --> " << x2 << endl;
		if (x2 < 0)
			div += 0.2;
	}
	
	div = 1/div;
	while (x2 + w2 > img.cols)
	{
		w2 = 3 * w/div;
		cout << "Adjusting width: " << w << " --> " << w2 << endl;
		div += 0.2;
	}
	
	div = 1;
	while (y2 < 0)
	{
		y2 = y - h/div;
		cout << "Adjusting top left y coord: " << y << " --> " << y2 << endl;
		if (y2 < 0)
			div += 0.2;
	}
	div = 1/div;
	while (y2 + h2 > img.rows)
	{
		h2 = 3 * h/div;
		cout << "Adjusting height: " << h << " --> " << h2 << endl;
		div += 0.2;
	}
	Rect crop_rect = Rect(x2, y2, w2, h2);
	Mat face_img = img(crop_rect);
	
	return face_img;
}
