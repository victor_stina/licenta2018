#include <iostream>
#include "../Headers/RServer.h"
#include "../Headers/FileSystemHandler.h"
#include "../Headers/FaceRecognition.h"
#include "../Headers/FaceDetector.h"
#include <signal.h>

using namespace std;

int main()
{
	signal(SIGCHLD, SIG_IGN);
	RServer server;
	server.event_loop();

	return 0;
}
