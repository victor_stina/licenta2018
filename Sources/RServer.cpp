#include "../Headers/RServer.h"
#include <iostream>
#include <string>
#include <string.h>
#include <sstream>
#include <thread>

using namespace std;

RServer::RServer()
{
	cout << "Initializing socket...\n";
	int result = socket_init();

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = INADDR_ANY;
	server_address.sin_port = htons(8888);

	cout << "Creating socket...\n";
	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address));
}

RServer::RServer(int port_number)
{
	int result = socket_init();
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = INADDR_ANY;
	server_address.sin_port = htons(port_number);

	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address));
}

RServer::~RServer()
{

}

result RServer::socket_init()
{
#ifdef _WIN32
	WSADATA wsa_data;
	return WSAStartup(MAKEWORD(1, 1), &wsa_data);
#else
	return OK_STATUS;
#endif
}

result RServer::socket_quit()
{
#ifdef _WIN32
	return WSACleanup();
#else
	return OK_STATUS;
#endif
}

result RServer::socket_close(SOCKET socket)
{
	int status = OK_STATUS;

#ifdef _WIN32
	status = shutdown(socket, SD_BOTH);
	if (status == OK_STATUS) { status = closesocket(socket); }
#else
	status = shutdown(socket, SHUT_RDWR);
	if (status == OK_STATUS) { status = close(socket); }
#endif

	return status;
}

void RServer::event_loop()
{
	cout << "Listenling...\n";
	listen(server_socket, 3);
	socklen_t c = sizeof(sockaddr_in);

	while (true)
	{
		client_socket = accept(server_socket, (sockaddr*) &client_address, &c);
		cout << "Connection accepted!\n";
		int pid = fork();
		if (pid == 0)
		{
			cout << "Launched process " << ::getpid() << " for handling the request\n";
			char buffer[256];
			result result = read_data(client_socket, buffer, 0);
			data_packet data = parse_packet(buffer);
			//cout << data.path << endl << data.action << endl << data.id << endl;
			cout << "Workspace found in: " << data.path << endl;
			cout << "User id: " << data.id << endl;
			if (result == OK_STATUS)
			{
				if (data.action == LOGIN_REQUEST)
				{
					cout << "Starting login request handler...\n";
					RecognitionHandler recognition_handler;
					int ret = recognition_handler.execute_recognition_sequence(data.path, data.id);
					if (ret == SUCCESS)
						send_data(client_socket, "0", 1);
					else if (ret == LOGIN_AUTH_ERROR)
						send_data(client_socket, "1", 1);
					else if (ret == LOGIN_PATH_ERROR)
						send_data(client_socket, "2", 1);
				}
				else if (data.action == REGISTER_REQUEST)
				{
					cout << "Starting register request handler...\n";
					RecognitionHandler recognition_handler;
					int ret = recognition_handler.register_user(data.path, data.id);
					if (ret == SUCCESS)
						send_data(client_socket, "0", 1);
					else if (ret == REGISTRATION_ERROR)
						send_data(client_socket, "3", 1);
				}
				else if (data.action == GENDER_REQUEST)
				{
					cout << "Starting gender request handler...\n";
					GenderClassifier gender_classifier;
					int ret = gender_classifier.classify_gender(data.path);
					//send_data(client_socket, (char*) std::to_string(ret).c_str(), 1);
					string cmd = "python ../Scripts/databaseQuery.py Gender " + std::to_string(ret) + ' ' + std::to_string(data.id);
					//std::thread genderThread(&RServer::exec, this, cmd.c_str());
					exec(cmd.c_str());
				}
				else if (data.action == AGE_REQUEST)
				{
					cout << "Starting age request handler...\n";
					AgeClassifier age_classifier;
					int ret = age_classifier.classify_age(data.path);
					//send_data(client_socket, (char*)std::to_string(ret).c_str(), 1);
					string cmd = "python ../Scripts/databaseQuery.py Age " + std::to_string(ret) + ' ' + std::to_string(data.id);
					//std::thread ageThread(&RServer::exec, this, cmd.c_str());
					exec(cmd.c_str());
				}
			}
			socket_close(client_socket);
			exit(0);
		}
		else
				close(client_socket);
	}

	socket_quit();
	socket_close(server_socket);
}

result RServer::read_data(SOCKET socket, char* buffer, int count)
{
	int bytesread = 0;
	result result;

	if (count == 0)
	{
		result = recv(socket, buffer, 256, 0);
		return OK_STATUS;
	}
	else
	{
		while (bytesread < count)
		{
			result = recv(socket, buffer + bytesread, count - bytesread, 0);
			if (result <= 0) { break; }
			bytesread += result;
		}

		return (result <= 0) ? SERVER_RECEIVE_DATA_ERROR : OK_STATUS;
	}
}

result RServer::send_data(SOCKET socket, char* buffer, int count)
{
	int bytesread = 0;
	int result;

	while (bytesread < count)
	{
		result = send(socket, buffer + bytesread, count - bytesread, 0);
		if (result <= 0) { break; }
		bytesread += result;
	}

	return (result <= 0) ? SERVER_SEND_DATA_ERROR : OK_STATUS;
}


data_packet RServer::parse_packet(char* input_string)
{
	data_packet data;
	stringstream iss;
	iss << input_string;
	string token;
	
	getline(iss, token, '#');
	data.path = token;
	
	getline(iss, token, '#');
	data.action = atoi(token.c_str());
	
	getline(iss, token, '#');
	data.id = atoi(token.c_str());
	
	return data;
}

string RServer::exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}
