#include "RecognitionHandler.h"
#include <vector>
#include <string>

using namespace std;
using namespace cv;

Mat norm_0_255(const Mat& src) 
{
    Mat dst;
    switch(src.channels()) 
	{
		case 1:
			cv::normalize(src, dst, 0, 255, NORM_MINMAX, CV_8UC1);
			break;
		case 3:
			cv::normalize(src, dst, 0, 255, NORM_MINMAX, CV_8UC3);
			break;
		default:
			src.copyTo(dst);
			break;
    }
    return dst;
}

RecognitionHandler::RecognitionHandler()
{
	face_recognizer.load();
}

int RecognitionHandler::execute_recognition_sequence(string path, int label)
{
	vector<string> subject_photos = FileSystemHandler::get_files_from_directory(path);
	int valid_count = 0;
	for (size_t index = 0; index < subject_photos.size(); index++)
	{
		Frame frame;
		frame.load_image(path + "/" + subject_photos[index]);
		frame.convert_to_gray();
		frame.resize(640, 480);
		vector<Mat> faces;
		face_detector.detect_faces(frame, faces);
		
		if (faces.size() == 0)
			return -100;
		
		Frame face_frame(faces[0]);
		face_frame = face_recognizer.process_img(face_frame.get_frame());
		face_frame.resize(92, 112);
		
		face_recognizer.predict(norm_0_255(face_frame.get_frame()));
		cout << "Prediction: " << face_recognizer.prediction << endl;
		cout << "Confidence: " << face_recognizer.confidence << endl;
		if (face_recognizer.prediction == label)
			valid_count++;
	}
	std::experimental::filesystem::remove_all(path);
	if (valid_count > 1)
		return 0;
	else
		return 1;
}

int RecognitionHandler::register_user(string path, int label)
{
	vector<string> subject_photos = FileSystemHandler::get_files_from_directory(path);
	vector<int> labels;
	vector<Mat> images;
	int error_count = 0;
	
	for (size_t index = 0; index < subject_photos.size(); index++)
	{
		Frame frame;
		frame.load_image(path + "/" + subject_photos[index]);
		frame.convert_to_gray();
		frame.resize(640, 480);
		
		vector<Mat> faces;
		face_detector.detect_faces(frame, faces);
		
		if (faces.size() == 0 || faces.size() > 1)
		{
			error_count++;
			continue;
		}
		Frame face_frame(faces[0]);
		face_frame = face_recognizer.process_img(face_frame.get_frame());
		face_frame.resize(92, 112);
		
		images.push_back(norm_0_255(face_frame.get_frame()));
		labels.push_back(label);
	}
	
	if (error_count > 5)
	{
		std::experimental::filesystem::remove_all(path);
		return 3;
	}
	face_recognizer.update(images, labels);
	face_recognizer.save();
	
	return 0;
}
