<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:300" rel="stylesheet">
	<link rel="stylesheet"  href="css/face_test.css">
	<link rel="stylesheet"  href="css/diverse.css">
	<script src="js/face_test.js"></script>

	<title>FaceRecog</title>
</head>
<body>
	<div id="whole-page-container">
		<div class="login-main-container">
			<h1 class="face-rec-header">Face Registration</h1>

			<video autoplay="true" id="videoElement"></video>
			<canvas id="canvas" width="640" height="480"></canvas>
			<canvas id="blank" width="640" height="480"></canvas>
			<p id="p1" style="color:red;text-align:center"></p>

			<button id="snap" type="button" name="face-registration">
				<div class="commence-face-rec-bttn-text">COMMENCE</div>
			</button>
		</div>
	</div>

	<script type="text/javascript">
		var video = document.querySelector("#videoElement");
 
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
		 
		if (navigator.getUserMedia) {       
			navigator.getUserMedia({video: true}, handleVideo, videoError);
		}
		function handleVideo(stream) {
		    video.src = window.URL.createObjectURL(stream);
		}
		 
		function videoError(e) {
		}
	</script>

	<script>
			document.getElementById("snap").addEventListener("click", function(e) {
			e.preventDefault(); 	
			var canvas = document.getElementById('canvas');
			document.getElementById('canvas').style.display = 'none';
			var canvas_blank = document.getElementById('blank');
			var context = canvas.getContext('2d');
			var no_of_pics = 0;
			while (no_of_pics < 3)
			{
				context.drawImage(video, 0, 0, 640, 480);
				var dataURL = canvas.toDataURL();
				if (dataURL == canvas_blank.toDataURL())
				{
					continue;
				}
				no_of_pics += 1;
				var xmlHttpReq = false;       
				if (window.XMLHttpRequest) {
					ajax = new XMLHttpRequest();
				}

				else if (window.ActiveXObject) {
					ajax = new ActiveXObject("Microsoft.XMLHTTP");
				}
				ajax.open('POST', 'uploaddata.php', false);
				ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				ajax.onreadystatechange = function() {
					console.log(ajax.responseText);
				}
				ajax.send("imgData="+dataURL+"l");	
			}
			ajax.open('POST', 'server_communication.php', false);
			ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			ajax.onload = function (e) {
				if (ajax.readyState === 4) {
					if (ajax.status === 200) {
						if (ajax.responseText == "Success") {
							window.location.replace("welcome.php");
						}
						else {
							document.getElementById("p1").innerHTML = "An error occured! Try again!";
							//document.write(ajax.responseText);
						}
					}
				}
			}
			ajax.send();
			});
	</script>
</body>
</html>
