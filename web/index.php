<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed:300" rel="stylesheet">
	<link rel="stylesheet"  href="css/login.css">
	<script src="js/login.js"></script>

	<title>Face Recognition</title>
</head>
<body>
	<div id="whole-page-container">
		<div class="login-main-container">
			<ul class="ul-login-tabs" type="none">
				<li id="sign-in" class="opacity-0" onclick="active_tab(1)">SIGN IN</li>
				<li id="sign-up" class="opacity-0" onclick="active_tab(2)">SIGN UP</li>
				<li id="recover" class="opacity-0" onclick="active_tab(3)">RECOVER</li>
			</ul>

			<div class="active-underline"></div> <!-- Linia albastra care sta sub tabul activ (signin up reg)-->

			 <form id="input_form" name ="input_form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
			 	<div id="email" class="input-block non-existent" onclick="active_input_box(3)">
					<input class="input_email_value" type="text" name="email_input" placeholder="" spellcheck="false" value=""  />
					<p class="email-text">Email</p>
				</div>

				<div id="username" class="input-block margin-top" onclick="active_input_box(1)">
					<input class="input_username_value" type="text" name="username_input" placeholder="" spellcheck="false" value="" />
					<p class="username-text">Username</p>
				</div>

			    <div id="password" class="input-block" onclick="active_input_box(2)">
				    <input class="input_password_value" type="password" name="password_input" placeholder="" spellcheck="false" value="" />
					<p class="password-text">Password</p>
				</div>
				<!-- firstname
				lastname
				gender
				age -->
				<div id="firstname" class="input-block non-existent" onclick="active_input_box(4)">
				    <input class="input_firstname_value" type="text" name="firstname_input" placeholder="" spellcheck="false" value="" />
					<p class="firstname-text">Firstname</p>
				</div>

				<div id="lastname" class="input-block non-existent" onclick="active_input_box(5)">
				    <input class="input_lastname_value" type="text" name="lastname_input" placeholder="" spellcheck="false" value="" />
					<p class="lastname-text">Lastname</p>
				</div>

				<div id="age" class="input-block non-existent" onclick="active_input_box(6)">
				    <input class="input_age_value" type="text" name="age_input" placeholder="" spellcheck="false" value="" />
					<p class="age-text">Age</p>
					<div class="linie-verticala"></div>
					<button type="button" class="age-autodetect-button" name="age-autodetect"> Autodetect </button> <!-- Ignori asta pentru php -->
					<input type="text" class="input-autodetect-age" name="autodetect-age-check" value="0" > <!-- Input invizibil, iei value din el daca e 1 sau 0 pentru autodetect -->
				</div>

				<div id="gender" class="input-block-gender non-existent" >
					<p class="gender-text">Gender</p>
					<button type="button" class="male-gender-button" name="male-gender-select"> Male </button>
					<button type="button" class="autodetect-gender-button" name="autodetect-gender-select"> Autodetect </button>
					<button type="button" class="female-gender-button" name="female-gender-select" onclick> Female </button>
					<input type="text" class="male-gender-input" name="male-hidden-input" value="0" >
					<input type="text" class="female-gender-input" name="female-hidden-input" value="0" > <!-- La astea 3 te uiti dupa value="0". Daca e 0 inseamna ca nu e selectat, 1 e selectat -->
					<input type="text" class="autodetect-gender-input" name="autodetect-hidden-input" value="0" >
					
				</div>
				

				<!-- ///////////////////////////////////////////////// -->

				<button id="continue-login-button" type="submit" name="signin">
					<div class="continue-text">CONTINUE</div>
				</button>
			</form> 
			<?php include 'php_login_script.php'; ?>
		</div>
	</div>
</body>
</html>
