var active_tab_global = 0; // Variable used to specify the current active tab signin/up/recov
var active_input_box_var = 0; // 1 = username ; 2 = password ; 3 = email ; 3 = firstname 5=lastname 6 = age 7 = gender


$(function(){

	first_container_appearance(0); // Sa apara loginu prima oara la load cu translateY si opacitate

	setTimeout( function(){ // at the begging to set SignIn active
		active_tab(1); 
	} , 300); // Daca schimbi timpul tre sa schimbi si de la  first_container_appearance() functia pentru active-underline

	var email = $("#email"); // merge stearsa dar... nu vreau sa o sterg ca poate o mai foloseste vreo linie de cod



	//Prima aparitie a containerului de login dupa load page
	//--------------------------------------------------------------------------------------->
	// x == 0 -> apare la load prima oara
	// x == orice altceva -> inseamna ca s-a dat a doua oara, cand s-a facut switchul de la stream inapoi la astea fara sa schimb borderu
	function first_container_appearance(x){
		if(x == 0)
			$(".login-main-container").css({
										"transform" : "translateY(40px) ",
										"opacity" : "1"
									 	 });
		
		setTimeout( function(){
				$("#sign-in").css({
								"transform" : "translateY(10px) ",
								"opacity" : "1",
								 });
		} , 200);

		setTimeout( function(){
				$("#sign-up").css({
								"transform" : "translateY(10px) ",
								"opacity" : "1"
								 });
		} , 300);


		setTimeout( function(){
				$("#recover").css({
								"transform" : "translateY(10px) ",
								"opacity" : "1"
								 });
		} , 400);


		setTimeout( function(){
				$(".active-underline").css({
								"transform" : "translateX(0px) ",
								"opacity" : "1",
								"border-color" : "#2CE5B7"
								 });
		} , 350);
	} 	
	// END function first_container_appearance(x){ 
	// ---------------------------------------------------------------------------------------->



	// Hover on username/password/signIn/sing-out/recover input boxes
	$("#username").hover( 
		function(){
			if(active_input_box_var != 1){
				$(this).css("border-color" , "#4f5663");
				$(".username-text").css("color" , "#707a8f");
			}
		}, function(){
			if(active_input_box_var != 1){
				$(this).css("border-color" , "#393e47");
				$(".username-text").css("color" , "#5a6272");
			}
		}
	);

	$("#password").hover( 
		function(){
			if(active_input_box_var != 2){
				$(this).css("border-color" , "#4f5663");
				$(".password-text").css("color" , "#707a8f");
			}
		}, function(){
			if(active_input_box_var != 2){
				$(this).css("border-color" , "#393e47");
				$(".password-text").css("color" , "#5a6272");
			}
		}
	);

	$("#email").hover( 
		function(){
			if(active_input_box_var != 3){
				$(this).css("border-color" , "#4f5663");
				$(".email-text").css("color" , "#707a8f");
			}
		}, function(){
			if(active_input_box_var != 3){
				$(this).css("border-color" , "#393e47");
				$(".email-text").css("color" , "#5a6272");
			}
		}
	);

	$("#firstname").hover( 
		function(){
			if(active_input_box_var != 4){
				$(this).css("border-color" , "#4f5663");
				$(".firstname-text").css("color" , "#707a8f");
			}
		}, function(){
			if(active_input_box_var != 4){
				$(this).css("border-color" , "#393e47");
				$(".firstname-text").css("color" , "#5a6272");
			}
		}
	);

	$("#lastname").hover( 
		function(){
			if(active_input_box_var != 5){
				$(this).css("border-color" , "#4f5663");
				$(".lastname-text").css("color" , "#707a8f");
			}
		}, function(){
			if(active_input_box_var != 5){
				$(this).css("border-color" , "#393e47");
				$(".lastname-text").css("color" , "#5a6272");
			}
		}
	);

	$("#age").hover( 
		function(){
			if(active_input_box_var != 6){
				$(this).css("border-color" , "#4f5663");
				$(".age-text").css("color" , "#707a8f");
				$(".linie-verticala").css("border-left-color" , "#4f5663");
			}
		}, function(){
			if(active_input_box_var != 6){
				$(this).css("border-color" , "#393e47");
				$(".age-text").css("color" , "#5a6272");
				$(".linie-verticala").css("border-left-color" , "#393e47");
			}
		}
	);

	$("#gender").hover( 
		function(){
				$(this).css("border-color" , "#4f5663");
				$(".gender-text").css("color" , "#707a8f");
				if($(".male-gender-button").css("color") == "#22B38F")
					$(".male-gender-button").css("color" , "#707a8f" );
				if($(".female-gender-button").css("color") == "#22B38F")
					$(".female-gender-button").css("color" , "#707a8f" );
				if($(".autodetect-gender-button").css("color") == "#22B38F")
					$(".autodetect-gender-button").css("color" , "#707a8f" );
		}, function(){
				$(this).css("border-color" , "#393e47");
				$(".gender-text").css("color" , "#5a6272");
				if($(".male-gender-button").css("color") == "#22B38F")
					$(".male-gender-button").css("color" , "#5a6272" );
				if($(".female-gender-button").css("color") == "#22B38F")
					$(".female-gender-button").css("color" , "#5a6272" );
				if($(".autodetect-gender-button").css("color") == "#22B38F")
					$(".autodetect-gender-button").css("color" , "#5a6272" );
		}
	);
	//-------------------------------------------------

	$("#sign-in").hover( 
		function(){
			if(active_tab_global != 1) // In case it's already active with color green
				$(this).css("color" , "#829ab0");
		}, function(){
			if(active_tab_global != 1)
				$(this).css("color" , "#d0d8e1");
		}
	);

	$("#sign-up").hover( 
		function(){
			if(active_tab_global != 2)
				$(this).css("color" , "#829ab0");
		}, function(){
			if(active_tab_global != 2)
					$(this).css("color" , "#d0d8e1");
		}
	);

	$("#recover").hover( 
		function(){
			if(active_tab_global != 3)
				$(this).css("color" , "#829ab0");
		}, function(){
			if(active_tab_global != 3)
				$(this).css("color" , "#d0d8e1");
		}
	);

	$("#continue-login-button").hover(
		function() {
				$(this).css("background-color" , "#3edab3");
		} , function(){
				$(this).css("background-color" , "#22B38F");
		});

	//Made to deactivate the input box if there is a click outside it
	$("html").click(function(){
		if(!$("#username").is(":hover") && !$("#password").is(":hover") && !$("#email").is(":hover") && !$("#firstname").is(":hover") && !$("#lastname").is(":hover") && !$("#age").is(":hover")){ // daca clickul nu este pe user parola sau mail	
			switch(active_input_box_var){
				case 1:{
					if($('.input_username_value').val() == '')
						username_text_LEFT_slide();
						$("#username").css("border-color" , "#393e47");
					break;
				}
				case 2:{
					if($('.input_password_value').val() == '')
						password_text_LEFT_slide();
						$("#password").css("border-color" , "#393e47");
					break;
				}
				case 3:{
					if($('.input_email_value').val() == '')
						email_text_LEFT_slide();
						$("#email").css("border-color" , "#393e47");	
					break;
				}
				case 4:{
					if($('.input_firstname_value').val() == '')
						firstname_text_LEFT_slide();
						$("#firstname").css("border-color" , "#393e47");
					break;
				}
				case 5:{
					if($('.input_lastname_value').val() == '')
						lastname_text_LEFT_slide();
						$("#lastname").css("border-color" , "#393e47");
					break;
				}
				case 6:{
					if($('.input_age_value').val() == '')
						age_text_LEFT_slide();
						$("#age").css("border-color" , "#393e47");
						$(".linie-verticala").css("border-color" , "#393e47");
					break;
				}
				case 7:{
						$("#gender").css("border-color" , "#393e47");
					break;
				}
			}
			active_input_box_var = 0;
		}
	});



	$(".age-autodetect-button").click(function(){    //cand butonul de la autodetect age e apasat
		if($(".input-autodetect-age").val() == "0")
		{
			$(".input-autodetect-age").val("1");
			$(".age-autodetect-button").css("color" , "#22B38F");
			$(".input_age_value").prop('disabled', true);
			$(".input_age_value").val("To be determined");
			$(".age-text").val("");
			age_text_RIGHT_slide();
		}
		else 
		{ 
			$(".input-autodetect-age").val("0");
			$(".age-autodetect-button").css("color" , "#5a6272");
			$(".input_age_value").prop('disabled', false);
			$(".input_age_value").val("");
			$(".age-text").val("Age");
			age_text_LEFT_slide();
		}
	});

	$(".male-gender-button").click(function(){    //cand butonul de la male gender e apasat
		if($(".male-gender-input").val() == "0")
		{
			$(".male-gender-input").val("1");
			$(".male-gender-button").css("color" , "#22B38F");
			$(".female-gender-input").val("0");
			$(".autodetect-gender-input").val("0"); //restul devin 0
			$(".female-gender-button, .autodetect-gender-button").css("color" , "#5a6272");

		}
		else 
		{ 
			$(".male-gender-input").val("0");
			$(".male-gender-button").css("color" , "#5a6272");

		}
	});

	$(".female-gender-button").click(function(){    //cand butonul de la female gender e apasat
		if($(".female-gender-input").val() == "0")
		{
			$(".female-gender-input").val("1");
			$(".female-gender-button").css("color" , "#22B38F"); //se activeaza culoarea
			$(".autodetect-gender-input").val("0"); //restul devin 0
			$(".male-gender-input").val("0");
			$(".autodetect-gender-button, .male-gender-button").css("color" , "#5a6272");

		}
		else 
		{ 
			$(".female-gender-input").val("0");
			$(".female-gender-button").css("color" , "#5a6272");

		}
	});

	$(".autodetect-gender-button").click(function(){    //cand butonul de la autodetect gender e apasat
		if($(".autodetect-gender-input").val() == "0")
		{
			$(".autodetect-gender-input").val("1");
			$(".male-gender-input").val("0");
			$(".female-gender-input").val("0");
			$(".autodetect-gender-button").css("color" , "#22B38F");
			$(".female-gender-button, .male-gender-button").css("color" , "#5a6272");

		}
		else 
		{ 
			$(".autodetect-gender-input").val("0");
			$(".autodetect-gender-button").css("color" , "#5a6272");

		}
	});

}); 
//END  $(function(){
//--------------------------------------------------------------------------------------->


//Function for selective the tab between signup signin recover
//--------------------------------------------------------------------------------------->
function active_tab(active){
	// Sa se stearga valoarea din input si sa revina la stanga eticheta in caz ca se schimba tabul
	$('.input_email_value').val('');
	$('.input_password_value').val('');
	$('.input_username_value').val('');
	$('.input_firstname_value').val('');
	$('.input_lastname_value').val('');
	$('.input_age_value').val('');
	email_text_LEFT_slide();
	password_text_LEFT_slide();
	username_text_LEFT_slide();
	firstname_text_LEFT_slide();
	lastname_text_LEFT_slide();
	age_text_LEFT_slide();
	$(".female-gender-input").val('0');
	$(".male-gender-input").val('0');
	$(".autodetect-gender-input").val('0');
	$(".female-gender-button").css("color" , "#5a6272");
	$(".male-gender-button").css("color" , "#5a6272");
	$(".autodetect-gender-button").css("color" , "#5a6272");


	switch(active){
		case 1:{ // sign-in active
			$('#continue-login-button').attr('name' , 'signin'); // Sa se schimbe numele inputului pentru php
			if(active_tab_global != 1){
				setTimeout( function(){
					$(".login-main-container").height(270);
				} , 350 ); // 550

				switch(active_tab_global){
					case 0: { // prima oara cand e sign-in
						setTimeout( function(){
							sign_IN_box_transition(2); // sa apara de sus in jos 
						} , 180 );
						break;
					}

					case 2: { // precedent a fost sign-up                
						setTimeout( function(){                    // -----------------de facut
							sign_UP_box_transition(1); // sa dispara sign up
						} , 100 );

						setTimeout( function(){
							sign_IN_box_transition(2); // sa apara de sus in jos 
						} , 700 ); // 600
						break;
					}

					case 3:{ // precedent a fost recover
						setTimeout( function(){                    // -----------------de facut
							recover_box_transition(1); // sa dispara recover
						} , 100 );

						setTimeout( function(){
							sign_IN_box_transition(2); // sa apara de sus in jos 
						} , 600 );
						break;
					}
					// NOTE : NU PUNE DROPUL DE LA SIGN IN JOS PENTRU CA DIFERA TIMPUL DE DISPARITIE DE LA FIECARE TAB
				}


				// Style colors and shit 
				$("#sign-in").css("color" , "#2CE5B7"); 
				$("#sign-up").css("color" , "#d0d8e1"); 
				$("#recover").css("color" , "#d0d8e1"); 

				// Sa se mute linia sub sign in 
				$(".active-underline").css({ 
											"transform" : "translateX(0px)",
											"width" : "75px"
										  });


				// Setari initiale
				// $("#email, #password, #username, #continue-login-button").css("transform" , transform + " scaleX(1)");
				$(".continue-text").html("CONTINUE");
				active_tab_global = 1;
			}
			
			break;
		}


		case 2:{ // sign-up active
			$('#continue-login-button').attr('name' , 'signup'); // Sa se schimbe numele inputului pentru php
			if (active_tab_global != 2) {
				setTimeout( function(){
					$(".login-main-container").height(560);
				} , 750 ); // 700 inainte sa fac modificari victor

				switch(active_tab_global){
					case 1: { // precedentul a fost sign-in
						setTimeout( function(){              
							sign_IN_box_transition(1); // sa dispara sign in
						} , 100 );

						setTimeout( function(){
							sign_UP_box_transition(2); // sa apara de sus in jos 
						} , 500 );
						break;
					}

					case 3:{ // precedent a fost recover
						setTimeout( function(){               
							recover_box_transition(1); // sa dispara recover
						} , 100 );

						setTimeout( function(){
							sign_UP_box_transition(2); // sa apara de sus in jos 
						} , 450 );
						break;
					}
					// NOTE : NU PUNE DROPUL DE LA SIGN IN JOS PENTRU CA DIFERA TIMPUL DE DISPARITIE DE LA FIECARE TAB
				}
				

				$("#sign-in").css("color" , "#d0d8e1"); 
				$("#sign-up").css("color" , "#2CE5B7"); 
				$("#recover").css("color" , "#d0d8e1"); 
				$(".active-underline").css({
											"transform" : "translateX(103px)",
											"width" : "80.18px"
										  });

				
				active_tab_global = 2;
			}
			break;
		}


		case 3:{ // recover active
			$('#continue-login-button').attr('name' , 'recover'); // Sa se schimbe numele inputului pentru php
			if(active_tab_global != 3){
				switch(active_tab_global){
					case 1: { // precedentul a fost sign-in
						setTimeout( function(){
							$(".login-main-container").height(220);
						} , 550 );

						setTimeout( function(){                
							sign_IN_box_transition(1); // sa dispara sign in
						} , 100 );

						setTimeout( function(){
							recover_box_transition(2); // sa apara de sus in jos 
						} , 400 ); // de refacut neaprat
						break;
					}

					case 2: { // precedentul este sign-up 
						setTimeout( function(){
							$(".login-main-container").height(220);
						} , 350 );

						setTimeout( function(){                 
							sign_UP_box_transition(1); // sa dispara sign in
						} , 100 );

						setTimeout( function(){
							recover_box_transition(2); // sa apara de sus in jos 
						} , 500 );
						break;           
					}
				}

				$("#sign-in").css("color" , "#d0d8e1"); 
				$("#sign-up").css("color" , "#d0d8e1"); 
				$("#recover").css("color" , "#2CE5B7"); 
				$(".active-underline").css({
											"transform" : "translateX(202px)",
											"width" : "94.739px"
										  });
				active_tab_global = 3;
			}
			break;
		}
	}
} // END  function active_tab(var active)
//--------------------------------------------------------------------------------------->





// Functie care muta cu boxurile de user parola cand esti pe signin
//--------------------------------------------------------------------------------------->
// direction == 1 ->left
// direction == 2 ->down
// direction == 3 ->right
function sign_IN_box_transition(direction){ 
	switch(direction){
		case 1:{ // disappear

			$("#username, #password, #email, #continue-login-button").css({
										"transition" : "opacity 0.3s",
										"transition-timing-function" : "cubic-bezier(.17,.67,.82,.98)"
									  });

			$("#continue-login-button").css({ // nu e nevoie de timer pentru ca dispare primul
								"opacity" : "0"
								 });

			// acum punem inapoi tranzitia de 1s sa fie gata pentru aparitie
		    // are timer ca sa se execute la un timp diferit fata de aia de sus
			setTimeout(function(){
				$("#continue-login-button").css({ 
								"transition" : "opacity 1s"
										    	 });
			} , 50);

			setTimeout(function(){
				$("#password").css({
								"opacity" : "0"
								 });
			} , 50);

			setTimeout(function(){
				$("#username").css({
								"opacity" : "0"
								 });
			} , 150);
			
			break;
		}

		case 2:{ // appear

			$("#email, #continue-login-button, #username, #password, #firstname, #lastname, #age, #gender").css({
											"transition" : "0"
										 });

			$("#username").addClass("margin-top");
			$("#email").removeClass("margin-top").addClass("non-existent");
			$("#firstname, #lastname, #gender, #age").addClass("non-existent");
			$("#username, #password").removeClass("non-existent");
			$("#username, #password, #email,#firstname, #lastname, #gender, #age, #continue-login-button").delay(10).css({ 
											"transition" : "transform 0.5s, opacity 0.5s", 
											"transition-timing-function" : "cubic-bezier(.17,.67,.82,.98)"
										 });

			setTimeout(function(){
				$("#username").css({
								"opacity" : "1"
								 });
			} , 150);
			

			setTimeout(function(){
				$("#password").css({
								"opacity" : "1"
								 });
			} , 250);

			setTimeout(function(){
				$("#continue-login-button").css({
								"opacity" : "1"
								 });
			} , 400);

			break;
		}
	}
}

// END function sign_IN_box_transition(direction)
//--------------------------------------------------------------------------------------->


function sign_UP_box_transition(direction){ 
	switch(direction){
		case 1:{ // disappear

			$("#username, #password, #email, #continue-login-button,  #firstname, #lastname, #age, #gender").css({
										"transition" : "opacity 0.3s",
										"transition-timing-function" : "cubic-bezier(.17,.67,.82,.98)"
									  });

			$("#continue-login-button").css({ 
								"opacity" : "0"
									    	 });

			setTimeout(function(){
				$("#gender").css({
								"opacity" : "0"
								 });
			} , 150);

			setTimeout(function(){
				$("#age").css({
								"opacity" : "0"
								 });
			} , 200	);

			setTimeout(function(){
				$("#lastname").css({
								"opacity" : "0"
								 });
			} , 250);

			setTimeout(function(){
				$("#firstname").css({
								"opacity" : "0"
								 });
			} , 300);

			setTimeout(function(){
				$("#password").css({
								"opacity" : "0"
								 });
			} , 350);

			setTimeout(function(){
				$("#username").css({
								"opacity" : "0"
								 });
			} , 400);

			setTimeout(function(){
				$("#email").css({
								"opacity" : "0"
								 });
			} , 450);
			break;
		}

		case 2:{ // appear

			$("#username, #password, #email, #firstname, #lastname, #age, #gender, #continue-login-button").css({
											"transition" : " "
										 });

			$("#username").removeClass("margin-top non-existent"); 
			$("#firstname, #lastname, #password, #age, #gender").removeClass("non-existent"); 
			$("#email").addClass("margin-top").removeClass("non-existent");

			$("#username, #password, #email, #firstname ,#lastname, #gender, #age, #continue-login-button").delay(10).css({ 
											"transition" : "transform 0.5s, opacity 0.5s", 
											"transition-timing-function" : "cubic-bezier(.17,.67,.82,.98)"
										 });

			setTimeout(function(){
				$("#email").css({
								"opacity" : "1"
								 });
			} , 150);

			setTimeout(function(){
				$("#username").css({
								"opacity" : "1"
								 });
			} , 250);
			

			setTimeout(function(){
				$("#password").css({
								"opacity" : "1"
								 });
			} , 350);

			setTimeout(function(){
				$("#firstname").css({
								"opacity" : "1"
								 });
			} , 450);

			setTimeout(function(){
				$("#lastname").css({
								"opacity" : "1"
								 });
			} , 550);

			setTimeout(function(){
				$("#age").css({
								"opacity" : "1"
								 });
			} , 650);

			setTimeout(function(){
				$("#gender").css({
								"opacity" : "1"
								 });
			} , 750);

			setTimeout(function(){
				$("#continue-login-button").css({
								"opacity" : "1"
								 });
			} , 850);
			break;
		}
	}
}


function recover_box_transition(direction){ 
	switch(direction){
		case 1:{ // disappear

			$("#username, #password, #email, #continue-login-button").css({
										"transition" : "opacity 0.3s",
										"transition-timing-function" : "cubic-bezier(.17,.67,.82,.98)"
									  });

			$("#continue-login-button").css({ 
								"opacity" : "0"
									    	 });


			setTimeout(function(){
				$("#continue-login-button").css({
								"opacity" : "0"
								 });
			} , 50);

			setTimeout(function(){
				$("#email").css({
								"opacity" : "0"
								 });
			} , 150);

			break;
		}

		case 2:{ // appear

			$("#email, #continue-login-button").css({
											"transition" : "0"
										 });

			$("#username").removeClass("margin-top");
			$("#email").addClass("margin-top");
			$("#email").removeClass("non-existent");
			$("#firstname, #lastname, #age, #gender").addClass("non-existent"); 
			$("#username, #password").addClass("non-existent");
			$("#username, #password, #email, #continue-login-button, #firstname, #lastname, #age, #gender").delay(10).css({ 
											"transition" : "transform 0.5s, opacity 0.5s", 
											"transition-timing-function" : "cubic-bezier(.17,.67,.82,.98)"
										 });

			setTimeout(function(){
				$("#email").css({
								"opacity" : "1"
								 });
			} , 150);


			setTimeout(function(){
				$("#continue-login-button").css({
								"opacity" : "1"
								 });
			} , 250);
			break;
		}
	}
}


//Function for activating the box input username/password/email
//--------------------------------------------------------------------------------------->
function active_input_box(active){
	if(active == 1) // username
	{ 
			switch(active_input_box_var){
				case 2:{
					$("#password").css("border-color" , "#393e47");
					if($('.input_password_value').val() == '')
					{
						password_text_LEFT_slide();
					}
					break;
				}
				case 3:{
					$("#email").css("border-color" , "#393e47");
					if($('.input_email_value').val() == '')
					{
						email_text_LEFT_slide();
					}
					break;
				}
				case 4:{
					$("#firstname").css("border-color" , "#393e47");
					if($('.input_firstname_value').val() == '')
					{
						firstname_text_LEFT_slide();
					}
					break;
				}
				case 5:{
					$("#lastname").css("border-color" , "#393e47");
					if($('.input_lastname_value').val() == '')
					{
						lastname_text_LEFT_slide();
					}
					break;
				}
				case 6:{
					$("#age").css("border-color" , "#393e47");
					$(".linie-verticala").css("border-left-color" , "#393e47");
					if($('.age_username_value').val() == '')
					{
						age_text_LEFT_slide();
					}
					break;
				}
				case 7:{
					$("#gender").css("border-color" , "#393e47");
					if($('.input_age_value').val() == '')
					{
						gender_text_LEFT_slide();
					}
					break;
				}
			}
			$("#username").css("border-color" , "#22B38F");
			username_text_RIGHT_slide(); // sa se mute textul username in dreapta
			active_input_box_var = 1;
	} 
	else if(active == 2) //password
	{ 
			switch(active_input_box_var){
				case 1:{
					$("#username").css("border-color" , "#393e47");
					if($('.input_username_value').val() == '')
					{
						username_text_LEFT_slide();
					}
					break;
				}
				case 3:{
					$("#email").css("border-color" , "#393e47");
					if($('.input_email_value').val() == '')
					{
						email_text_LEFT_slide();
					}
					break;
				}
				case 4:{
					$("#firstname").css("border-color" , "#393e47");
					if($('.input_firstname_value').val() == '')
					{
						firstname_text_LEFT_slide();
					}
					break;
				}
				case 5:{
					$("#lastname").css("border-color" , "#393e47");
					if($('.input_lastname_value').val() == '')
					{
						lastname_text_LEFT_slide();
					}
					break;
				}
				case 6:{
					$("#age").css("border-color" , "#393e47");
					$(".linie-verticala").css("border-left-color" , "#393e47");
					if($('.age_username_value').val() == '')
					{
						age_text_LEFT_slide();
					}
					break;
				}
				case 7:{
					$("#gender").css("border-color" , "#393e47");
					if($('.input_age_value').val() == '')
					{
						gender_text_LEFT_slide();
					}
					break;
				}
			}
			
			$("#password").css("border-color" , "#22B38F");
			password_text_RIGHT_slide(); // sa se mute textul password in dreapta
			active_input_box_var = 2;
	} else if(active == 3) // email
	{ 
		switch(active_input_box_var){
				case 1:{
					$("#username").css("border-color" , "#393e47");
					if($('.input_username_value').val() == '')
					{
						username_text_LEFT_slide();
					}
					break;
				}
				case 2:{
					$("#password").css("border-color" , "#393e47");
					if($('.input_password_value').val() == '')
					{
						password_text_LEFT_slide();
					}
					break;
				}
				case 4:{
					$("#firstname").css("border-color" , "#393e47");
					if($('.input_firstname_value').val() == '')
					{
						firstname_text_LEFT_slide();
					}
					break;
				}
				case 5:{
					$("#lastname").css("border-color" , "#393e47");
					if($('.input_lastname_value').val() == '')
					{
						lastname_text_LEFT_slide();
					}
					break;
				}
				case 6:{
					$("#age").css("border-color" , "#393e47");
					$(".linie-verticala").css("border-left-color" , "#393e47");
					if($('.input_age_value').val() == '')
					{
						age_text_LEFT_slide();
					}
					break;
				}
			}
			$("#email").css("border-color" , "#22B38F");
			email_text_RIGHT_slide(); // sa se mute textul password in dreapta
			active_input_box_var = 3;

	}else if(active == 4){ //firstname
		switch(active_input_box_var){
				case 1:{
					$("#username").css("border-color" , "#393e47");
					if($('.input_username_value').val() == '')
					{
						username_text_LEFT_slide();
					}
					break;
				}
				case 2:{
					$("#password").css("border-color" , "#393e47");
					if($('.input_password_value').val() == '')
					{
						password_text_LEFT_slide();
					}
					break;
				}
				case 3:{
					$("#email").css("border-color" , "#393e47");
					if($('.input_email_value').val() == '')
					{
						email_text_LEFT_slide();
					}
					break;
				}
				case 5:{
					$("#lastname").css("border-color" , "#393e47");
					if($('.input_lastname_value').val() == '')
					{
						lastname_text_LEFT_slide();
					}
					break;
				}
				case 6:{
					$("#age").css("border-color" , "#393e47");
					$(".linie-verticala").css("border-left-color" , "#393e47");
					if($('.input_age_value').val() == '')
					{
						age_text_LEFT_slide();
					}
					break;
				}
			}
			
			$("#firstname").css("border-color" , "#22B38F");
			firstname_text_RIGHT_slide(); // sa se mute textul password in dreapta
			active_input_box_var = 4;

	}else if(active == 5) //lastname
	{
		switch(active_input_box_var){
				case 1:{
					$("#username").css("border-color" , "#393e47");
					if($('.input_username_value').val() == '')
					{
						username_text_LEFT_slide();
					}
					break;
				}
				case 2:{
					$("#password").css("border-color" , "#393e47");
					if($('.input_password_value').val() == '')
					{
						password_text_LEFT_slide();
					}
					break;
				}
				case 3:{
					$("#email").css("border-color" , "#393e47");
					if($('.input_email_value').val() == '')
					{
						email_text_LEFT_slide();
					}
					break;
				}
				case 4:{
					$("#firstname").css("border-color" , "#393e47");
					if($('.input_firstname_value').val() == '')
					{
						firstname_text_LEFT_slide();
					}
					break;
				}
				case 5:{
					$("#lastname").css("border-color" , "#393e47");
					if($('.input_lastname_value').val() == '')
					{
						lastname_text_LEFT_slide();
					}
					break;
				}
				case 6:{
					$("#age").css("border-color" , "#393e47");
					$(".linie-verticala").css("border-left-color" , "#393e47");
					if($('.input_age_value').val() == '')
					{
						age_text_LEFT_slide();
					}
					break;
				}
			}
			$("#lastname").css("border-color" , "#22B38F");
			lastname_text_RIGHT_slide(); // sa se mute textul password in dreapta
			active_input_box_var = 5;
	}else if(active == 6) //age
	{
		switch(active_input_box_var){
				case 1:{
					$("#username").css("border-color" , "#393e47");
					if($('.input_username_value').val() == '')
					{
						username_text_LEFT_slide();
					}
					break;
				}
				case 2:{
					$("#password").css("border-color" , "#393e47");
					if($('.input_password_value').val() == '')
					{
						password_text_LEFT_slide();
					}
					break;
				}
				case 3:{
					$("#email").css("border-color" , "#393e47");
					if($('.input_email_value').val() == '')
					{
						email_text_LEFT_slide();
					}
					break;
				}
				case 4:{
					$("#firstname").css("border-color" , "#393e47");
					if($('.input_firstname_value').val() == '')
					{
						firstname_text_LEFT_slide();
					}
					break;
				}
				case 5:{
					$("#lastname").css("border-color" , "#393e47");
					if($('.input_lastname_value').val() == '')
					{
						lastname_text_LEFT_slide();
					}
					break;
				}
			}
			$("#age").css("border-color" , "#22B38F");
			$(".linie-verticala").css("border-left-color" , "#22B38F");

			if(!$(".age-autodetect-button").is(":hover"))  // sa se dea in dreapta doar daca nu e apasat butonul de autodetect, caz in care sa ramana asa
				age_text_RIGHT_slide(); // sa se mute textul password in dreapta
			active_input_box_var = 6;
	}else if(active == 7) //gender
	{
		switch(active_input_box_var){
				case 1:{
					$("#username").css("border-color" , "#393e47");
					if($('.input_username_value').val() == '')
					{
						username_text_LEFT_slide();
					}
					break;
				}
				case 2:{
					$("#password").css("border-color" , "#393e47");
					if($('.input_password_value').val() == '')
					{
						password_text_LEFT_slide();
					}
					break;
				}
				case 3:{
					$("#email").css("border-color" , "#393e47");
					if($('.input_email_value').val() == '')
					{
						email_text_LEFT_slide();
					}
					break;
				}
				case 4:{
					$("#firstname").css("border-color" , "#393e47");
					if($('.input_firstname_value').val() == '')
					{
						firstname_text_LEFT_slide();
					}
					break;
				}
				case 5:{
					$("#lastname").css("border-color" , "#393e47");
					if($('.input_lastname_value').val() == '')
					{
						lastname_text_LEFT_slide();
					}
					break;
				}
				case 6:{
					$("#age").css("border-color" , "#393e47");
					$(".linie-verticala").css("border-left-color" , "#393e47");
					if($('.input_age_value').val() == '')
					{
						age_text_LEFT_slide();
					}
					break;
				}
			}
			active_input_box_var = 0;
	}
}
// END function active_input_box(active)
//--------------------------------------------------------------------------------------->




// Functii folosite pentru a face slide right si left la textul din boxul de username sau password
//--------------------------------------------------------------------------------------->
function username_text_RIGHT_slide(){
	$(".username-text").css({
							"transform" : "translateX(40px)",
							"opacity" : "0"
				  			});
}

function username_text_LEFT_slide(){
	$(".username-text").css({
							"transform" : "translateX(0px)",
							"opacity" : "1",
							"color" : "#5a6272"
						   });
}

function password_text_RIGHT_slide(){
	$(".password-text").css({
							"transform" : "translateX(40px)",
							"opacity" : "0",
						   });
}

function password_text_LEFT_slide(){
	$(".password-text").css({
							"transform" : "translateX(0px)",
							"opacity" : "1",
							"color" : "#5a6272"
						   });
}

function email_text_RIGHT_slide(){
	$(".email-text").css({
							"transform" : "translateX(40px)",
							"opacity" : "0"
						});
}

function email_text_LEFT_slide(){
	$(".email-text").css({
							"transform" : "translateX(0px)",
							"opacity" : "1",
							"color" : "#5a6272"
					    });
}

function firstname_text_RIGHT_slide(){
	$(".firstname-text").css({
							"transform" : "translateX(40px)",
							"opacity" : "0"
				  			});
}

function firstname_text_LEFT_slide(){
	$(".firstname-text").css({
							"transform" : "translateX(0px)",
							"opacity" : "1",
							"color" : "#5a6272"
						   });
}

function lastname_text_RIGHT_slide(){
	$(".lastname-text").css({
							"transform" : "translateX(40px)",
							"opacity" : "0"
				  			});
}

function lastname_text_LEFT_slide(){
	$(".lastname-text").css({
							"transform" : "translateX(0px)",
							"opacity" : "1",
							"color" : "#5a6272"
						   });
}

function age_text_RIGHT_slide(){
	$(".age-text").css({
							"transform" : "translateX(40px)",
							"opacity" : "0"
				  			});
}

function age_text_LEFT_slide(){
	$(".age-text").css({
							"transform" : "translateX(0px)",
							"opacity" : "1",
							"color" : "#5a6272"
						   });
}



//End functii text slide
//--------------------------------------------------------------------------------------->




